# eslint-config-ifinsg
A basic React TypeScript eslint config.

## Installation
```
npm i -D git+https://ifinsgdev:adEarxiHzdokUpUiKRHB@bitbucket.org/ifinsg/eslint-config-ifinsg.git
```

## Usage
Add to your eslint config `.eslintrc`, or `eslintConfig` field in `package.json` :

```json
{
    "extends": "ifinsg"
}
```

## Settings (VSCode)
Make sure the following JSON is added into your VSCode settings file :
```
"eslint.validate": ["typescript", "typescriptreact"],
```
(Optional) If you need VSCode to auto fix linting errors for you, add the following into your vscode settings :
```
"editor.codeActionsOnSave": {
    "source.fixAll.eslint": true 
}
```
